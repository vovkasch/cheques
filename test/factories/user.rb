FactoryGirl.define do
  factory :user do
  	sequence(:name) { |i| "User #{i}"}
  	sequence(:email) { |i| "user#{i}@example.com"}

    trait :employee do
      role { create :role, :employee }
    end

    trait :unconfirmed do
      role { create :role, :unconfirmed }
    end

    trait :admin do
      role { create :role, :admin }
    end
  end
end
