class Exporter
  attr_accessor :client
  private :client

  def initialize
    @client = HomeFinanceAPI::Client.new JsonApi::Request.new
  end

  def export(records)
    records = records.map { |r| Decorator.new r }
    client.send_outcome records.map &:data
    records.each &:exported!
  end

private

  class Decorator < SimpleDelegator
    def data
      { date: date,
        comment: comment,
        amount: amount,
        currency_id: currency_id,
        category_id: category_id,
        account_id: account_id }
    end

  private

    def date
      super.to_time.to_i
    end
  end
end
