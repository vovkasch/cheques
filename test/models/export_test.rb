require 'test_helper'

class ExportTest < ActiveSupport::TestCase
  test "#initializes as not persisted" do
    export = Export.new

    assert_equal export.persisted?, false
  end

  test '#initializes with all not exported checks' do
    not_exported_check = create :check
    exported_check = create :check, :exported

    export = Export.new

    assert_equal export.checks, [ not_exported_check ]
    refute_equal export.checks, [ exported_check ]
  end

  test '#initializes with some not exported checks' do
    not_exported_check_1 = create :check
    not_exported_check_2 = create :check

    export = Export.new [ not_exported_check_1.id ]

    assert_equal export.checks, [ not_exported_check_1 ]
    refute_equal export.checks, [ not_exported_check_2 ]
  end

  test '#save exports each passed check' do
    not_exported_check = create :check
    export = Export.new [ not_exported_check.id ]
    exporter = export.send(:exporter)
    def exporter.export(checks)
      @exported_checks = checks
    end

    export.save

    assert_equal exporter.instance_variable_get('@exported_checks'), [ not_exported_check ]
  end

  test '#save does not persist export object' do
    export = Export.new

    export.save

    assert_equal export.persisted?, false
  end
end
