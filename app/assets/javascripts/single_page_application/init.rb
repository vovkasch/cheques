require 'active_support'

Document.on 'ready page:load' do
  PagesFactory.build.run
end

Document.on 'ready' do
  `Turbolinks.enableProgressBar()`
end
