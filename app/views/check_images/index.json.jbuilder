json.array!(@check_images) do |check_image|
  json.extract! check_image, :id
  json.url check_image_url(check_image, format: :json)
end
