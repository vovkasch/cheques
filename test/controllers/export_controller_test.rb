require 'test_helper'

class ExportsControllerTest < ActionController::TestCase
  setup do
    sign_in create :user, :admin
  end

  test "should get index" do
    get :index

    assert_response :success
    assert_not_nil assigns(:export)
  end

  test "should create export" do
    post :create, export: { check_ids: [] }

    assert_redirected_to exports_path(assigns(:export))
  end
end
