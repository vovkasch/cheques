class Users::MessagesController < ApplicationController
  filter_access_to :all

  def wait_for_confirmation
    flash[:alert] = ''
    flash[:warning] = I18n.t(:'devise.failure.unconfirmed')
  end
end
