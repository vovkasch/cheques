class CreateCheckImages < ActiveRecord::Migration
  def change
    create_table :check_images do |t|
      t.string :image
      t.timestamps
    end
  end
end
