# == Schema Information
#
# Table name: check_images
#
#  id         :integer          not null, primary key
#  image      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class CheckImageTest < ActiveSupport::TestCase
  test ".with_checks" do
    with_check = create :check_image, :with_check
    without_check = create :check_image

    assert_includes CheckImage.with_checks, with_check
    refute_includes CheckImage.with_checks, without_check
  end

  test ".without_checks" do
    with_check = create :check_image, :with_check
    without_check = create :check_image

    assert_includes CheckImage.without_checks, without_check
    refute_includes CheckImage.without_checks, with_check
  end

  test '.not_exported' do
    with_exported_check = create :check_image, :exported
    with_not_exported_check = create :check_image, :with_check
    without_check = create :check_image

    assert_includes CheckImage.not_exported, with_not_exported_check
    assert_includes CheckImage.not_exported, without_check
    refute_includes CheckImage.not_exported, with_exported_check
  end
end
