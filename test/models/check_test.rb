# == Schema Information
#
# Table name: checks
#
#  id             :integer          not null, primary key
#  date           :date             not null
#  comment        :text(1023)
#  amount         :decimal(8, 2)
#  category_id    :integer          not null
#  currency_id    :integer          not null
#  account_id     :integer          not null
#  check_image_id :integer          not null
#  user_id        :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#  exported       :boolean          default(FALSE)
#

require 'test_helper'

class CheckTest < ActiveSupport::TestCase
  test "#exported!" do
    check = create :check

    check.exported!

    assert_equal check.exported, true
  end

  test '.not_exported' do
    not_exported = create :check, exported: false
    exported = create :check, exported: true

    assert_includes Check.not_exported, not_exported
    refute_includes Check.not_exported, exported
  end
end
