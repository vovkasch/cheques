module DateServices
  module Format
    class Builder
      attr_reader :pattern
      private :pattern

      def initialize(pattern)
        @pattern = pattern
      end

      def build
        format_type.new(pattern)
      end

      private

        def format_type
          case true
          when ruby?
            DateServices::Format::RubyApp
          when datepicker?
            DateServices::Format::Datepicker
          else
            DateServices::Format::Nil
          end
        end

        def ruby?
          pattern.include? '%'
        end

        def datepicker?
          !ruby?
        end
    end
  end
end
