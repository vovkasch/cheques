#= require_tree ./../pages

class PagesFactory
  def self.build
    memoize(controller_name, action_name) { (page_class.nil? ? NullPage : page_class).new }
  end

  def self.page_class
    modul = `self._scope[#{ controller_name.camelize }] || {}`
    klass = `modul[#{ action_name.camelize }] || nil`
  end

  def self.controller_name
    body.css_classes[0]
  end

  def self.action_name
    body.css_classes[1]
  end

  def self.body
    HTMLElement.new('body')
  end

  def self.memoize(controller_name, action_name, &block)
    @memos ||= {}
    @memos[controller_name] ||= {}
    @memos[controller_name][action_name] ||= block.call
  end
end
