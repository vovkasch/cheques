class Page
  def initialize
    puts "Initialized #{self.class.name}"
  end

  def run
    puts "#{self.class.name}#run"
  end

  def on(event, selector, callback_name)
    `$(document).on(event, selector, self['$'+callback_name].bind(self))`
  end
end
