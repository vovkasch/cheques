authorization do
  role :guest do
    # add permissions for guests here, e.g.
    # has_permission_on :conferences, :to => :read
  end

  role :unconfirmed do
    has_permission_on :users_messages, to: :wait_for_confirmation
  end

  role :employee do
    has_permission_on :check_images, to: %i( read update )
    has_permission_on :checks, to: :create
    has_permission_on :checks, to: :update do
      if_attribute user_id: is { user.id }
    end
  end

  role :admin do
    has_permission_on :check_images, to: :manage
    has_permission_on :checks, to: :manage
    has_permission_on :roles, to: :manage
    has_permission_on :exports, to: :manage
    has_permission_on :import_checks, to: :manage
  end

  # permissions on other roles, such as
  # role :admin do
  #   has_permission_on :conferences, :to => :manage
  # end
  # role :user do
  #   has_permission_on :conferences, :to => [:read, :create]
  #   has_permission_on :conferences, :to => [:update, :delete] do
  #     if_attribute :user_id => is {user.id}
  #   end
  # end
  # See the readme or GitHub for more examples
end

privileges do
  # default privilege hierarchies to facilitate RESTful Rails apps
  privilege :manage, :includes => [ :create, :read, :update, :delete, :import_checks ]
  privilege :read, :includes => [:index, :show]
  privilege :create, :includes => [ :new, :create ]
  privilege :update, :includes => [ :edit, :update ]
  privilege :delete, :includes => :destroy
  privilege :import_checks, :includes => :dropbox
end
