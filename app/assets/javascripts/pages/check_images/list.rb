module CheckImages
  class List < Page
    def run
      on 'change', '#check_image_image', :start_upload
      on 'click', '.upload-image', :open_file
      super
    end

  private

    def start_upload
      `$('#new_check_image').submit()`
    end

    def open_file(event)
      `event.preventDefault()`
      `#{file_button}.trigger('click')`
    end

    def file_button
      Element.find '#check_image_image'
    end
  end
end
