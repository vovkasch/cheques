require 'test_helper'

class Users::MessagesControllerTest < ActionController::TestCase
  test "should get wait_for_confirmation" do
    unconfirmed_user = create :user, :unconfirmed
    sign_in unconfirmed_user

    get :wait_for_confirmation, id: unconfirmed_user.to_param

    assert_response :success
  end

  test "'confirmed user can't get wait_for_confirmation page" do
    confirmed_user = create :user, :employee
    sign_in confirmed_user

    get :wait_for_confirmation, id: confirmed_user.to_param

    assert_redirected_to root_path
  end
end
