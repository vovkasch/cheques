class ExportsController < ApplicationController
  respond_to :html

  def index
    @export = Export.new
  end

  def create
    @export = Export.new export_params[:check_ids]
    @export.save
  rescue HomeFinanceAPI::Client::NoConnectionException => ex
    flash[:error] = 'It\'s a pity:( Seems we have no connection at the moment and can\'t send your data to server. '
  ensure
    respond_with @export
  end

  private

    def export_params
      params.require(:export)
            .permit(check_ids: [])
            .tap { |this| this['check_ids'].reject! { |i| i == '0' } }
    end
end
