module CheckDecorator
  def categories_options
    Category.all.map { |category| [category.name, category.id] }
  end

  def currencies_options
    Currency.all.map { |currency| [currency.name, currency.id] }
  end

  def accounts_options
    Account.all.map { |account| [account.name, account.id] }
  end

  def datepicker_date(format=I18n.t('date.formats.default'))
    (date.present? ? date : DateTime.now.to_date).strftime(format)
  end

  def datepicker_format(pattern=I18n.t('date.formats.default').dup, date_format_builder=DateServices::Format::Builder)
    date_format_builder.new(pattern).build
                                    .replace_year.replace_month.replace_day
                                    .to_s
  end

  delegate :image, to: :check_image

  def mini_image_url
    image.mini.url
  end

  def image_url
    image.url
  end
end
