class Export
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def persisted?
    persisted
  end

	attr_reader :checks, :check_ids
  attr_accessor :persisted
  private :persisted, :persisted=

	def initialize(check_ids=nil)
		@checks = check_ids.blank? ? Check.not_exported : Check.not_exported.where(id: check_ids)
    @persisted = false
  end

  def save
    exporter.export checks
  end

  private

  def exporter
    @exporter ||= Exporter.new
  end
end
