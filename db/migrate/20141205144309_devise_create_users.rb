class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      t.string :email,                           default: ""
      t.string :name,               null: false, default: ""
      t.datetime :remember_created_at
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      t.string :provider
      t.string :uid
      t.belongs_to :role, null: false
      t.timestamps
    end

    add_index :users, :email,                unique: true
  end
end
