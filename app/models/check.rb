# == Schema Information
#
# Table name: checks
#
#  id             :integer          not null, primary key
#  date           :date             not null
#  comment        :text(1023)
#  amount         :decimal(8, 2)
#  category_id    :integer          not null
#  currency_id    :integer          not null
#  account_id     :integer          not null
#  check_image_id :integer          not null
#  user_id        :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#  exported       :boolean          default(FALSE)
#

class Check < ActiveRecord::Base
  #schema_validations
  #schema_associations

  belongs_to :category
  belongs_to :currency
  belongs_to :account
  belongs_to :check_image
  belongs_to :user

  [ :category, :account, :currency ].each { |attribute_name|
    delegate :name, to: attribute_name, prefix: true }

  scope :not_exported, -> { where exported: false }

  def exported!
    update_attribute :exported, true
  end
end
