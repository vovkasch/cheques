class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    provider :fb
  end

  def vkontakte
    provider :vk
  end

private

  def provider(name)
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
      set_flash_message(:notice, :success, kind: name) if is_navigational_format?
    else
      session["devise.#{name}_data"] = request.env["omniauth.auth"]
    end
    sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
  end
end
