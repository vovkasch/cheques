require 'test_helper'

class ChecksControllerTest < ActionController::TestCase
  setup do
    @employee = create(:user, :employee)
    sign_in @employee
  end

  def date
    DateTime.now.strftime( I18n.t('date.formats.default') )
  end

  def valid_params(defaults={})
    @valid_params ||= { date:           date,
                        comment:        'Lorem ipsum dolor sit amet',
                        amount:         '123.456',
                        category_id:    (defaults[:category]    || create(:category)).to_param,
                        currency_id:    (defaults[:currency]    || create(:currency)).to_param,
                        account_id:     (defaults[:account]     || create(:account)).to_param,
                        check_image_id: (defaults[:check_image] || create(:check_image)).to_param }
  end

  test "should create check" do
    assert_difference('Check.count') do
      post :create, check: valid_params
    end

    assert_redirected_to edit_check_image_path(valid_params[:check_image_id])
  end

  test "should update check" do
    check = create :check, user: @employee

    patch :update, id: check, check: valid_params(check_image: check.check_image_id)

    assert_redirected_to edit_check_image_path(check.check_image)
  end

  test "can't update others check" do
    check = create :check

    patch :update, id: check, check: valid_params

    assert_redirected_to root_path
  end
end
