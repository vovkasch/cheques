json.array!(@checks) do |check|
  json.extract! check, :id, :date, :comment, :amount, :category_id, :currency_id, :account_id
  json.url check_url(check, format: :json)
end
