class ChecksController < ApplicationController
  before_action :set_check, only: [:update]

  # POST /checks.json
  def create
    @check = Check.new check_params
    @check.user = current_user

    respond_to do |format|
      if @check.save
        format.html { redirect_to edit_check_image_path(@check.check_image), notice: 'Check was successfully created.' }
        format.json { render :show, status: :created, location: @check }
      else
        format.html { render edit_check_image_path(@check.check_image) }
        format.json { render json: @check.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /checks/1
  # PATCH/PUT /checks/1.json
  def update
    respond_to do |format|
      if @check.update check_params
        format.html { redirect_to edit_check_image_path(@check.check_image), notice: 'Check was successfully updated.' }
        format.json { render :show, status: :ok, location: @check }
      else
        format.html { render edit_check_image_path(@check.check_image) }
        format.json { render json: @check.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_check
      @check = Check.find params[:id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def check_params
      prepared_params.require(:check)
                     .permit(:date, :comment, :amount, :category_id, :currency_id, :account_id, :image, :check_image_id)
    end

    def prepared_params
      params[:check].merge! date: DateServices::Converter.new(params[:check][:date]).parse
      params
    end
end

