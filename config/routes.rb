require 'resque/server'

Rails.application.routes.draw do
  mount Resque::Server.new, at: '/resque' if Rails.env.development?

  resource :import_checks, only: :index do
    get :index
    post :dropbox
  end

  resource :exports, only: %i( create ) do
    get :index
  end

  namespace :users do
    get ':id/wait_for_confirmation', to: 'messages#wait_for_confirmation', as: :wait_for_confirmation
  end

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new', as: :new_user_session
    get 'sign_out', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  root to: 'check_images#index'

  resources :check_images, except: %i( show )
  resources :checks, only: %i( create update )
  resources :roles
end
