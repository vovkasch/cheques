require 'test_helper'

class CheckImagesControllerTest < ActionController::TestCase
  setup { sign_in create :user, :admin }

  test "should get index" do
    create :check_image

    get :index

    assert_response :success
  end

  test 'should get not exported check image' do
    check_image = create :check_image

    get :index

    assert_includes assigns(:check_images), check_image
  end

  test 'should not show exported check images' do
    exported_check_image = create :check_image, :exported

    get :index

    refute_includes assigns(:check_images), exported_check_image
  end

  test "should get new" do
    get :new

    assert_response :success
  end

  test "should create check_image" do
    assert_difference('CheckImage.count') do
      post :create, check_image: {  }
    end

    assert_redirected_to check_images_path
  end

  test "should get edit" do
    check_image = create :check_image

    get :edit, id: check_image

    assert_response :success
  end

  test "should update check_image" do
    check_image = create :check_image

    patch :update, id: check_image, check_image: {  }

    assert_redirected_to check_image_path(assigns(:check_image))
  end

  test "should destroy check_image" do
    check_image = create :check_image

    assert_difference('CheckImage.count', -1) do
      delete :destroy, id: check_image
    end

    assert_redirected_to check_images_path
  end
end
