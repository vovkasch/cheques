class HTMLElement
  attr_reader :element
  private :element

  def initialize(selector)
    @element = Element.find(selector)
  end

  def css_classes
    `self.element[0].className.split(' ')`
  end
end
