module CheckImageDecorator
  def new_check
    @new_check ||= Check.new.tap { |check| check.check_image_id = self.id }
  end

  def thumb_url
    @thumb_url ||= rescue_not_found { image.thumb.url }
  end

  def preview_url
    @preview_url ||= rescue_not_found { image.preview.url }
  end

  def image_url
    @image_url ||= rescue_not_found { image.url }
  end

  private

    def rescue_not_found(&block)
      yield if block_given?
    rescue DropboxError
      nil
    end
end
