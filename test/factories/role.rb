FactoryGirl.define do
  factory :role do
  	name {}

    trait :employee do
      name 'Employee'
    end

    trait :unconfirmed do
      name 'Unconfirmed'
    end

    trait :admin do
      name 'admin'
    end
  end
end
