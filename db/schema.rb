# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150114000612) do

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",               default: ""
    t.string   "name",                default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.integer  "role_id",                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], :name => "index_users_on_email", :unique => true
    t.index ["role_id"], :name => "fk__users_role_id"
    t.foreign_key ["role_id"], "roles", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_users_role_id"
  end

  create_table "accounts", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], :name => "fk__accounts_user_id"
    t.foreign_key ["user_id"], "users", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_accounts_user_id"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], :name => "fk__categories_user_id"
    t.foreign_key ["user_id"], "users", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_categories_user_id"
  end

  create_table "check_images", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "currencies", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], :name => "fk__currencies_user_id"
    t.foreign_key ["user_id"], "users", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_currencies_user_id"
  end

  create_table "checks", force: true do |t|
    t.date     "date",                                                                null: false
    t.text     "comment",        limit: 1023
    t.decimal  "amount",                      precision: 8, scale: 2
    t.integer  "category_id",                                                         null: false
    t.integer  "currency_id",                                                         null: false
    t.integer  "account_id",                                                          null: false
    t.integer  "check_image_id",                                                      null: false
    t.integer  "user_id",                                                             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "exported",                                            default: false
    t.index ["account_id"], :name => "fk__checks_account_id"
    t.index ["category_id"], :name => "fk__checks_category_id"
    t.index ["check_image_id"], :name => "fk__checks_check_image_id"
    t.index ["currency_id"], :name => "fk__checks_currency_id"
    t.index ["user_id"], :name => "fk__checks_user_id"
    t.foreign_key ["account_id"], "accounts", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_checks_account_id"
    t.foreign_key ["category_id"], "categories", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_checks_category_id"
    t.foreign_key ["check_image_id"], "check_images", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_checks_check_image_id"
    t.foreign_key ["currency_id"], "currencies", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_checks_currency_id"
    t.foreign_key ["user_id"], "users", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_checks_user_id"
  end

end
