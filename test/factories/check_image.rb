FactoryGirl.define do
	factory :check_image do
    check nil

    trait :with_check do
      after(:create) do |check_image, _|
        create :check, check_image: check_image
      end
    end

    trait :exported do
      after(:create) do |check_image, _|
        create :check, :exported, check_image: check_image
      end
    end
	end
end
