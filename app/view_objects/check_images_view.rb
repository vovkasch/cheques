class CheckImagesView < SimpleDelegator
  def without_checks
    check_images = super
    preload_thumb_images check_images
    check_images
  end

  def with_checks
    check_images = super
    preload_thumb_images check_images
    check_images
  end

  private

    def preload_thumb_images(check_images)
      check_images.map { |check_image|
        check_image = check_image.extend CheckImageDecorator
        Thread.new {
          check_image.thumb_url
        }
      }.each &:join
    end
end
