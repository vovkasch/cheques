class AddExportedToChecks < ActiveRecord::Migration
  def change
    add_column :checks, :exported, :boolean, default: false
  end
end
