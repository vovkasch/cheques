class CreateChecks < ActiveRecord::Migration
  def change
    create_table :checks do |t|
      t.date :date, null: false
      t.text :comment, limit: 1023
      t.decimal :amount, precision: 8, scale: 2
      t.belongs_to :category, null: false
      t.belongs_to :currency, null: false
      t.belongs_to :account, null: false
      t.belongs_to :check_image, null: false
      t.belongs_to :user, null: false

      t.timestamps
    end
  end
end
