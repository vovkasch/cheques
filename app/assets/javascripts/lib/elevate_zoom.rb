class ElevateZoom
  attr_reader :selector, :options
  private :selector, :options

  def initialize
    @selector = 'img[data-zoom-image]'
    @options = { zoomType: 'lens',
                 lensShape: 'round',
                 lensSize: 300,
                 scrollZoom: true
                #zoomWindowPosition: "demo-container",
                #zoomWindowHeight: 200,
                #zoomWindowWidth:200,
                #borderSize: 0,
                #easing: true
              }
  end

  def apply
    `#{images}.elevateZoom(#{options}.map)`
  end

  def images
    Element.find selector
  end
end
