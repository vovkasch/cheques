ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/pride'
require 'minitest/mock'

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods
end

class MiniTest::Unit::TestCase
end

class ActionController::TestCase
  include Devise::TestHelpers
  include FactoryGirl::Syntax::Methods
end
