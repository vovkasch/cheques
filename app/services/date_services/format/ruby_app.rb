module DateServices
  module Format
    class RubyApp
      attr_reader :pattern
      private :pattern

      def initialize(pattern)
        @pattern = pattern
      end

      def replace_year
        pattern.sub! '%Y', 'yyyy'
        self
      end

      def replace_month
        pattern.sub! '%m', 'mm'
        pattern.sub! '%b', 'M'
        self
      end

      def replace_day
        pattern.sub! '%d', 'dd'
        self
      end

      def to_s
        pattern
      end
    end
  end
end
