class ResqueJobs::DropboxImport
  @queue = :regular

  def self.perform
    dropbox_client = DropboxToolbox.client
    dropbox_client.import_new_checks!
  end
end
