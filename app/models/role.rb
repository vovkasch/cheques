# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Role < ActiveRecord::Base
  schema_validations
  schema_associations

  ROLE_NAMES = %i( guest unconfirmed employee admin )

  ROLE_NAMES.each do |name|
    scope name.to_s.pluralize, -> { where Role.arel_table[:name].matches(name) }
  end

  class << self
    ROLE_NAMES.each do |name|
      define_method(name) { send(name.to_s.pluralize).first }
    end
  end

  def confirmed?
    name.downcase != unconfirmed_name
  end

  private

    def unconfirmed_name
      unconfirmed = self.class.unconfirmed
      unconfirmed = (unconfirmed.respond_to?(:first) ? unconfirmed.first : unconfirmed)
      unconfirmed.present? ? unconfirmed.name.downcase : nil
    end
end
