class ImportChecksController < ApplicationController
  respond_to :html

  def dropbox
    Resque.enqueue ResqueJobs::DropboxImport
    flash[:success] = 'Checks import has been queued. '
    redirect_to root_path
  end
end
