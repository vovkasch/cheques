# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test '#confirmed?' do
    create :role, name: 'unconfirmed'
    role = create :role, name: 'Any other except `unconfirmed`'

    assert_equal true, role.confirmed?
  end

  test '#confirmed? fails' do
    role = create :role, name: 'unconfirmed'

    assert_equal false, role.confirmed?
  end

  test '#confirmed? fails for capitalized name' do
    role = create :role, name: 'Unconfirmed'

    assert_equal false, role.confirmed?
  end
end
