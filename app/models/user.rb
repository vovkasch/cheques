# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  email               :string(255)      default("")
#  name                :string(255)      default(""), not null
#  remember_created_at :datetime
#  sign_in_count       :integer          default(0), not null
#  current_sign_in_at  :datetime
#  last_sign_in_at     :datetime
#  current_sign_in_ip  :string(255)
#  last_sign_in_ip     :string(255)
#  provider            :string(255)
#  uid                 :string(255)
#  role_id             :integer          not null
#  created_at          :datetime
#  updated_at          :datetime
#

class User < ActiveRecord::Base
  schema_validations
  schema_associations

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :rememberable, :trackable, :omniauthable,
         :omniauth_providers => [ :facebook, :vkontakte ]

  delegate :confirmed?, to: :role

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name   # assuming the user model has a name
      user.role = Role.unconfirmed
    end
  end

  def role_symbols
    [role.name.downcase.to_sym]
  end
end
