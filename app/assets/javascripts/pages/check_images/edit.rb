module CheckImages
  class Edit < Page
    attr_reader :elevate_zoom
    private :elevate_zoom

    def initialize
      @elevate_zoom = ElevateZoom.new
      super
    end

    def run
      elevate_zoom.apply
      super
    end
  end
end
