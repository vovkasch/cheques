class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name, uniq: true

      t.timestamps
    end
  end
end
