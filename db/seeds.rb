# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create name: 'My First Category' unless Category.any?
Currency.create name: 'UAH' unless Currency.any?
Account.create name: 'My First Account' unless Account.any?
%i( guest unconfirmed employee admin ).each { |name| Role.create name: name } unless Role.any?

