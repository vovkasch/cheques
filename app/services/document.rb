class Document
  attr_reader :controller_name, :action_name
  private :controller_name, :action_name

  def initialize(controller_name, action_name)
    @controller_name = controller_name
    @action_name = action_name
  end

  def body_class
    "#{controller_token} #{action_token}"
  end

  private

    def controller_token
      controller_name
    end

    ACTION_TOKENS = { 'index' => 'list' }
    
    def action_token
      ACTION_TOKENS[action_name] || action_name
    end
end
