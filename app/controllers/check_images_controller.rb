class CheckImagesController < ApplicationController
  before_action :set_check_image, only: [:edit, :update, :destroy]

  # GET /check_images
  # GET /check_images.json
  def index
    @check_images = CheckImagesView.new CheckImage.not_exported
    @check_image = CheckImage.new
  end

  # GET /check_images/1/edit
  def edit
  end

  # POST /check_images
  # POST /check_images.json
  def create
    @check_image = CheckImage.new
    @check_image.image = check_image_params[:image]

    respond_to do |format|
      if @check_image.save
        format.html { redirect_to check_images_path, notice: 'Check image was successfully created.' }
        format.json { render :show, status: :created, location: @check_image }
      else
        format.html { render :new }
        format.json { render json: @check_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /check_images/1
  # PATCH/PUT /check_images/1.json
  def update
    params[:check_image] = {} if params[:check_image].blank?
    respond_to do |format|
      if check_image_params.present?
        if @check_image.update_attribute(:image, check_image_params[:image])
          format.html { redirect_to @check_image, notice: 'Check image was successfully updated.' }
          format.json { render :show, status: :ok, location: @check_image }
        else
          format.html { render :edit }
          format.json { render json: @check_image.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to @check_image, notice: 'Check image was not updated.' }
        format.json { render :show, status: :ok, location: @check_image }
      end
    end
  end

  # DELETE /check_images/1
  # DELETE /check_images/1.json
  def destroy
    @check_image.destroy
    respond_to do |format|
      format.html { redirect_to check_images_url, notice: 'Check image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def check_image_params
      (params[:check_image].presence || empty_params).permit(:image).presence || empty_params
    end

    def set_check_image
      @check_image = if params.include? :id
        CheckImage.where(id: params[:id]).first
      else
        CheckImage.new check_image_params
      end
    end

    def empty_params
      ActionController::Parameters.new({})
    end
end
