class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  filter_resource_access

  rescue_from Authorization::NotAuthorized do |exception|
    redirect_to root_path, alert: I18n.t(:'authorization.unauthorized')
  end

  def after_sign_in_path_for(resource_or_scope=nil)
    current_user.confirmed? ? root_path : users_wait_for_confirmation_path(current_user)
  end

  def new_session_path(scope)
    new_user_session_path(scope)
  end

  def permission_denied
    current_user.confirmed? ? raise(Authorization::NotAuthorized) : redirect_to(users_wait_for_confirmation_path(current_user))
  end

  module DeclarativeAuthorizationWithDeviseReconciled
    def load_controller_object(*args)
      super(*args) if authorizable_controller?
    end

    def filter_access_filter
      super if authorizable_controller?
    end

    def new_blank_controller_object(*args)
      super(*args) if authorizable_controller?
    end

  private

    def inauthorizable_controllers
      [ Users::OmniauthCallbacksController, Devise::SessionsController ]
    end

    def authorizable_controller?
      !inauthorizable_controllers.include? self.class
    end
  end
  include DeclarativeAuthorizationWithDeviseReconciled
end
