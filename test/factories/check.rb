FactoryGirl.define do
  factory :check do
    check_image
    category
    currency
    account
    user { create :user, :employee }
    date { DateTime.now }

    trait :exported do
      exported true
    end
  end
end
