# == Schema Information
#
# Table name: check_images
#
#  id         :integer          not null, primary key
#  image      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class CheckImage < ActiveRecord::Base
  schema_associations

  mount_uploader :image, CheckImageUploader

  has_one :check, dependent: :destroy

  scope :with_checks, -> { where(id: Check.pluck(:check_image_id)) }
  scope :without_checks, -> { where.not(id: Check.pluck(:check_image_id)) }
  scope :not_exported, -> { includes(:check).where(no_check.or(check_not_exported))
                                            .references(:check) }

  def self.no_check
    Check.arel_table[:id].eq(nil)
  end

  def self.check_not_exported
    Check.arel_table[:exported].eq(false)
  end
end
