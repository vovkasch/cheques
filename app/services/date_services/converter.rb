module DateServices
  class Converter
    attr_reader :date
    private :date

    def initialize(date)
      @date = date
    end

    def parse(format=I18n.t('date.formats.default'))
      date_parsed? ? date : Date.strptime(date, format)
    end

    private

      def date_parsed?
        date.blank? || date.is_a?(Date)
      end
  end
end
