namespace :cheque_images do
  task upload_new: :environment do
    client = DropboxToolbox.client
    client.handle_each_file_in_folder('/new') do |remote_file_metadata, local_file|
      check_image = CheckImage.new.tap { |check_image| check_image.image = local_file }.save
      client.remove remote_file_metadata
    end
  end
end
