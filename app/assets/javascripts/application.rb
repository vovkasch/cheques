#= require 'opal_ujs'
#= require_tree ./single_page_application
#= require_tree ./lib

require 'jquery.elevatezoom'
require 'turbolinks'
require 'bootstrap-datepicker'
require 'bootstrap-sprockets'
