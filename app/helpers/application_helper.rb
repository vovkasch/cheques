module ApplicationHelper
  def body_class
    Document.new(controller.controller_name, controller.action_name).body_class
  end
end
