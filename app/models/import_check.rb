class ImportCheck
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def persisted?
    false
  end

  def self.find(*args)
    new
  end
end
